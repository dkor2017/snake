// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->AddPoints();
			SpawnFood();
			Destroy();
		}
	}
}

void AFood::SpawnFood()
{
	if(foodElement)
	{
		FVector NewLocation(RandomFloat(minX,maxX), RandomFloat(minY, maxY) ,0);
		FTransform NewTransform = FTransform(NewLocation);
		AFood* newFood = GetWorld()->SpawnActor<AFood>(foodElement, NewTransform);
	}
}

float AFood::RandomFloat(float a, float b)
{
	float random = ((float) rand()) / (float) RAND_MAX;
	float diff = b - a;
	float r = random * diff;
	return a + r;
}

