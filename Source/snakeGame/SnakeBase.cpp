// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	points = 0;
	nextLevel = 10;
	ElementSize = 60.0f;
	wasBorned = false;
	LastMoveDirection = EMovementDirection::Up;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(5);
	wasBorned = true;
	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	IsTickBlocked = false;

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0;i < ElementsNum;i++)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize,0 ,0);
		if(wasBorned)
		{
			NewLocation = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
		}
		FTransform NewTransform = FTransform(NewLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementBase, NewTransform);
		NewSnakeElement->SnakeOwner=this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElement);
		if(ElemIndex == 0)
		{
			NewSnakeElement->SetFirstElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0,0,0);
	UE_LOG(LogTemp, Warning, TEXT("before switch mov %f %f"), ElementSize, MovementVector.X );
	switch (LastMoveDirection)
	{
		case EMovementDirection::Up:
			MovementVector.X -= ElementSize;
			break;
		case EMovementDirection::Down:
			MovementVector.X += ElementSize;
			break;
		case EMovementDirection::Left:
			MovementVector.Y -= ElementSize;
			break;
		case EMovementDirection::Right:
			MovementVector.Y += ElementSize;
			break;
	}
	//AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggelColission();
	UE_LOG(LogTemp, Warning, TEXT("after switch move %f %f"), ElementSize, MovementVector.X );
	for(int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i-1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	SnakeElements[0]->SetActorLocation(SnakeElements[0]->GetActorLocation() + MovementVector);
	SnakeElements[0]->ToggelColission();
}

void ASnakeBase::SetMovementDirection(EMovementDirection direction)
{
	if(!IsTickBlocked)
	{
		LastMoveDirection = direction;
		IsTickBlocked = true;
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::IncreaseSpeed()
{
	MovementSpeed *=0.9;
	SetActorTickInterval(MovementSpeed);
}

void ASnakeBase::AddPoints(int count)
{
	points += count;
	if (points > nextLevel)
	{
		nextLevel*=2;
		IncreaseSpeed();
	}
}

